<?php
/**
 * Home
 *
 * @package   Controllers
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace App\Controllers;

use App\Models\FileModel;
use App\Controllers\BaseController;
use Config\Services;
use \Modules\Users\Controllers\Users;

/**
 * Home
 *
 * @package        Controllers
 * @author         SISA Dev Team
 * @copypermission 2021 SISA
 * @license        https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class Home extends BaseController
{
	/**
	 * Index
	 *
	 * @return view The view to display
	 */
	public function index()
	{
		$session = session();

		if ($session->has('access_token'))
		{
			return redirect()->to('services');
		}
		else
		{
			return redirect()->to('login');
		}
	}

}
