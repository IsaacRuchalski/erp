<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;

class Menu extends BaseConfig
{
	public $menu = [
		[
			'label'    => 'Root',
			'icon'     => 'folder',
			'href'     => 'home',
			'children' => [

				[
					'label'    => 'Récents',
					'icon'     => 'folder',
					'href'     => 'recents',
					'children' => null,
				],

				[
					'label'    => 'Partagés',
					'icon'     => 'folder',
					'href'     => 'shared',
					'children' => null,
				],

			],

		],
		[

			'label'    => 'Corbeille',
			'icon'     => 'delete',
			'href'     => 'bin',
			'children' => null,

		],
	];

}
