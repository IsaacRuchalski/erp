<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Meta tags -->

  <?= $this->renderSection('head') ?>
</head>
<body class="with-custom-webkit-scrollbars with-custom-css-scrollbars" data-dm-shortcut-enabled="true" data-sidebar-shortcut-enabled="true" data-set-preferred-mode-onload="true">
  <!-- Modals go here -->
  <!-- Reference: https://www.gethalfmoon.com/docs/modal -->

  <!-- Page wrapper start -->
  <div class="page-wrapper with-navbar with-sidebar">

    <!-- Sticky alerts (toasts), empty container -->
    <!-- Reference: https://www.gethalfmoon.com/docs/sticky-alerts-toasts -->
    <div class="sticky-alerts"></div>

    <!-- Navbar start -->
    <nav class="navbar">
      <!-- Reference: https://www.gethalfmoon.com/docs/navbar -->
    </nav>
    <!-- Navbar end -->

    <!-- Sidebar start -->
    <div class="sidebar">
	<?= $this->renderSection('sidebar') ?>
      <!-- Reference: https://www.gethalfmoon.com/docs/sidebar -->
    </div>
    <!-- Sidebar end -->

    <!-- Content wrapper start -->
    <div class="content-wrapper">

	<?= $this->renderSection('content') ?>

	
    </div>
    <!-- Content wrapper end -->
	
  </div>
  <!-- Page wrapper end -->
  
  <!-- Halfmoon JS -->
  <script src="https://cdn.jsdelivr.net/npm/halfmoon@1.1.1/js/halfmoon.min.js"></script>
  
</body>
</html>
<style>

:root{

--primary-color: #33cc00;                                     /* Before: var(--blue-color) */



}

ul li {

list-style : none;

}

ul li p:before {}

</style>