<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class AuthTokens extends Seeder
{
	public function run()
	{
		$data = [
			[
				'access_token' => 'd437aa04d8f25aef9ee09719f822b85f',
				'user_id'      => 1,
			],
			[
				'access_token' => 'd437aa04d8f25aef9ee09719f822b85g',
				'user_id'      => 2,
			],

		];
		
		$this->db->table('auth_tokens')->insertBatch($data);
	}
}
