<?php
/**
 * Users
 *
 * @package   Seeds
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

/**
 * Users
 *
 * @package   Seeds
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class Users extends Seeder
{
	/**
	 * Run
	 *
	 * @return void
	 */
	public function run()
	{
		$data = [
			[

				'firstname' => 'Isaac',
				'lastname'  => 'Ruchalski',
				'email'     => 'isaac.ruchalski@gmail.com',
				'password'  => password_hash('Ruchalski!3105', PASSWORD_DEFAULT),
				'comment'   => '',
				'status'    => 0,
				'rank'      => 1,
				'ui_color'  => '#01dfd7',
			],
			[

				'firstname'       => 'Test',
				'lastname'        => 'Test2',
				'email'           => 'test.test@test.com',
				'password'        => password_hash('Motdepasse!123', PASSWORD_DEFAULT),
				'comment'         => '',
				'status'          => 0,
				'rank'            => 1,
				'ui_color'  => '#33cc00',
			],
		];

		$this->db->table('users')->insertBatch($data);
	}
}
