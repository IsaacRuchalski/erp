<?php
/**
 * File
 *
 * @package   Seeds
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

/**
 * File
 *
 * @package   Seeds
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class File extends Seeder
{
	/**
	 * Run
	 *
	 * @return void
	 */
	public function run()
	{
		/*	$data = [
			[
				'content' => 'Le contenu 0',
				'folder'  => 'myfiles',
			],
			[
				'content' => 'Le contenu 1',
				'folder'  => 'myfiles',
			],
			[
				'content' => 'Le contenu 2,recent',
				'folder'  => 'recents',
			],
			[
				'content' => 'Le contenu 3,corbeille',
				'folder'  => 'bin',
			],
			[

				'content' => 'Le contenu 4,partage',
				'folder'  => 'shared',
			],
		];

		$this->db->table('files')->insertBatch($data);*/
	}
}
