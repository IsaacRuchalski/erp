<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AuthTokens extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'token_id'     => [
				'type'           => 'INT',
				'null'           => false,
				'auto_increment' => true,

			],
			'access_token' => [
				'type'       => 'VARCHAR',
				'constraint' => 512,
				'null'       => false,
			],
			'user_id'      => [
				'type'           => 'INT',
				'constraint'     => 11,
				'unsigned'       => true,
				'auto_increment' => false,
			],
		]);

		$this->forge->addPrimaryKey('token_id');
		$this->forge->addKey('access_token', false);

		$this->forge->createTable('auth_tokens', true);

	}

	public function down()
	{
		$this->forge->dropTable('auth_tokens', true, true);
	}
}
