<?php
/**
 * Users
 *
 * @package   Migrations
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

/**
 * Users
 *
 * @package   Migrations
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class Users extends Migration
{
	/**
	 * Up
	 *
	 * @return void
	 */
	public function up()
	{

		
		$this->forge->addField([
			'user_id'   => [
				'type'           => 'INT',
				'constraint'     => 11,
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'firstname' => [
				'type'       => 'VARCHAR',
				'constraint' => '100',
				'null'       => true,
			],
			'lastname'  => [
				'type'       => 'VARCHAR',
				'constraint' => '100',
				'null'       => true,
			],
			'email'     => [
				'type'       => 'VARCHAR',
				'constraint' => '512',
			],
			'password'  => [
				'type'       => 'VARCHAR',
				'constraint' => '255',
				'null'       => true,
			],
			'comment'   => [
				'type' => 'TEXT',
				'null' => true,
			],
			'status'    => [
				'type'       => 'INT',
				'constraint' => 1,
				'default'    => 1,
			],
			'rank'      => [
				'type'     => 'INT',
				'unsigned' => true,
				'null'     => true,
			],
			'ui_color'  => [
				'type'       => 'VARCHAR',
				'constraint' => '255',
			],
		]);

	
		$this->forge->addKey('user_id', true);
		$this->forge->dropTable('users',true);
		$this->forge->createTable('users',true);
		
	}

	/**
	 * Down
	 *
	 * @return void
	 */
	public function down()
	{
		$this->forge->dropTable('users');
	}
}
