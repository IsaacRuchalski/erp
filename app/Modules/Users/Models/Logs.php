<?php
/**
 * Users
 *
 * @package   Users\Models
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace Modules\Users\Models;

use CodeIgniter\Model;

/**
 * Users
 *
 * @package   Users\Models
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class Logs extends Model
{

	/**
	 * Table the table to use
	 *
	 * @var string
	 */
	protected $table = 'logs';
	/**
	 * AllowedFields
	 * The allowed fields inside the table
	 *
	 * @var array
	 */
	protected $allowedFields = [
		'id',
		'address',
		'time',
	];

	/**
	 * GetLogsCount
	 * Gets how many logs there are for an IP
	 *
	 * @param mixed $log The address
	 *
	 * @return array An int representing the number of logs with the time of last connection of IP
	 */
	public function getLogsInfo(mixed $log)
	{
		{
			return $this->selectCount('host(address)', 'attempts')->selectMax('time')->where(['host(address)' => $log])->first();
		}
	}

	/**
	 * CreateLog
	 * Creates a log to the table
	 *
	 * @return void
	 */
	public function createLog()
	{
		$this->resetQuery();

		$data = [
			'address' => $this->getClientIpAddress(),
			'time'    => 'NOW()',
		];
		$this->insert($data);
		$response = [
			'status'   => 201,
			'error'    => null,
			'messages' => [
				'success' => 'Data Saved',
			],
		];
	}

	/**
	 * RemoveLog
	 * Removes a log from the table
	 *
	 * @return void
	 */
	public function removeLogs()
	{
		$this->resetQuery();
		$addr = $this->getClientIpAddress();
		$this->where('host(address)', $this->getClientIpAddress())->delete();
	}

	/**
	 * RegisterFailure
	 *
	 * @param mixed $addr The address
	 *
	 * @return void Nothing
	 */
	public function registerFailure(mixed $addr)
	{
		//	$flag = $this->select('address')->from('failures')->where('address', $addr)->getCompiledSelect();
		//	return $flag;
	}

	/**
	 * GetClientIpAddress
	 *
	 * @return string The IP
	 */
	public function getClientIpAddress()
	{
		if (! empty($_SERVER['HTTP_CLIENT_IP']))   //Checking IP From Shared Internet
		{
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (! empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //To Check IP is Pass From Proxy
		{
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		return $ip;
	}

}
