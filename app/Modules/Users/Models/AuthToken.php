<?php
/**
 * AuthToken
 * Model for token handling & API calls
 *
 * @package   Users\Models
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace Modules\Users\Models;

use CodeIgniter\Model;
use Modules\Users\Models\User;

/**
 * AuthToken
 * Model for token handling & API calls
 *
 * @package   Users\Models
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class AuthToken extends Model
{
	/**
	 * Table
	 * Table used by the model
	 *
	 * @var string
	 */
	protected $table = 'auth_tokens';
	 /**
	  * PrimaryKey
	  *
	  * @var string
	  */
	protected $primaryKey = 'access_token';

	/**
	 * AllowedFields
	 *
	 * @var array
	 */
	protected $allowedFields = [
		'access_token',
		'user_id',
	];


	/**
	 * HandleAccessToken
	 * Creates tokens or updates them
	 *
	 * @param mixed $data Data containing access token and user_id
	 *
	 * @return array $data Data you used
	 */
	public function handleAccessToken(mixed $data)
	{

		$val = $this->set('access_token', $data['access_token'])
		->where('user_id', $data['user_id']);

		if (null !== $this->where(['user_id' => $data['user_id']])->first())
		{
			$this->set('access_token', $data['access_token'])->where('user_id', $data['user_id'])
			->update();
		}
		else
		{
			$this->insert($data);
		}

		return $data;
	}
	/**
	 * FindAccessToken
	 * Finds token from mail and password
	 *
	 * @param mixed $data Mail and password
	 *
	 * @return array $res Token, and request datetime
	 */
	public function findAccessToken(mixed $data)
	{
		$user   = new User();
		$userDb = $user->find($data['email']);

		$token = '';
		$pass  = password_verify($data['password'], $userDb['password']);
		if ($pass && isset($userDb))
		{
			 $token = $this->asArray()
			 ->where('user_id', $userDb['user_id'])
			 ->first()['access_token'];
		}

		$res['access_token'] = $token;
		$res['at']           = new \DateTime;
		return $res;
	}

	/**
	 * GetUser
	 * Get user credentials from token
	 *
	 * @param array $data Array with access_token
	 *
	 * @return array $array Array with all attributes about user
	 */
	public function getUser(array $data)
	{
		$array  = [];
		$user   = new User();
		$userId = $this->find($data['access_token']);
		if (null !== $userId)
		{
			$array = $user->asArray()
			->where('user_id', $userId['user_id'])
			->first();
		}
		return $array !== null ? $array : $data;
	}

	/**
	 * CheckAccessToken
	 * Checks if access token exists
	 *
	 * @param mixed $data Access token
	 *
	 * @return void
	 */
	public function checkAccessToken(mixed $data)
	{
		$val = $this->asArray()->selectCount('access_token')->where('access_token', $data['access_token'])->getCompiledSelect();

		return $val;
	}
}
