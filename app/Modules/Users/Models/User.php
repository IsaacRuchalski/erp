<?php
/**
 * Users
 *
 * @package   Users\Models
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace Modules\Users\Models;

use CodeIgniter\Model;

/**
 * Users
 *
 * @package   Users\Models
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class User extends Model
{

	/**
	 * Table the table to use
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $allowedFields = [
		'email',
		'password',
		'user_id',
		'ui_color',
	];

	public function find($email = false, $pass = false)
	{
		$arr = null;
		if ($email === false)
		{
			$arr = $this->findAll();
		}

		if ($pass)
		{
			$arr = $this->asArray()
			->where(['email' => $email, 'password' => $pass])
			->first();
		}
		else
		{
			$arr = $this->asArray()
			->where(['email' => $email])
			->first();
		}

		return $arr;
	}

	public function updateByMail($mail, $password)
	{
		$this->set('password', $password)->where('email', $mail)->update();
	}

	public function exists($mail)
	{
		return $this->find($mail) !== null ? true : false;
	}

	public function getUserColor($mail){

		$arr = $this->asArray()->select('ui_color')
		->where(['email' => $mail])
		->first();

	}

	public function setUserColor($mail, $color){


		$this->set('ui_color', $color)->where('email', $mail)->update();

	}
}
