<?php
/**
 * Users
 *
 * @package   Users\Controllers
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace Modules\Users\Controllers;

use CodeIgniter\RESTful\ResourceController;

use \Modules\Users\Libraries\OAuth;
use \Modules\Users\Models\Logs;
use \Modules\Users\Models\User;
use \Modules\Users\Models\AuthToken;
use \OAuth2\Request;
use CodeIgniter\API\ResponseTrait;
use \Modules\Users\Libraries\Error;
use Modules\Services\Models\Config;

/**
 * Users
 *
 * @package   Users\Controllers
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class Users extends ResourceController
{

	use ResponseTrait;

	/**
	 * Index
	 *
	 * @return view Views for form (mail and pwd)
	 */
	public function index()
	{
		helper('cookie');
		set_cookie('sisa_token', '', time() - 3600);
		//At least 1 number, 1 special character,  8 chars min , 1 capital
		$restrictions = [
			'email'    => [
				'rules'  => 'required|valid_email',
				'errors' => [
					'valid_email' => 'Adresse e-mail non valide.',
					'required'    => 'Adresse e-mail requise.',
				],
			],
			'password' => [
				'rules'  => 'required|regex_match[/^\S*(?=\S{8})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\W])(?=\S*[\d])\S*$/]',
				'errors' => [
					'required'    => 'Mot de passe requis.',
					'regex_match' => 'Le mot de passe doit contenir au moins 1 majuscule, 8 caractères, et un caractère spécial.',
				],
			],
		];

		$steps = [
			'email',
			'password',
		];
		helper(['form']);
		$model  = new Logs();
		$essais = [
			3,
			2,
			1,
		];

		if (! empty($this->request->getPost()))
			{
			$data['attempts'] = $essais[$values = $model->getLogsInfo($this->request->getIPAddress())['attempts']];
			$verif            = $this->request->getPost('field');
			$data['email']    = $this->request->getPost('email');
			$data['password'] = $this->request->getPost('password');
			$step             = $verif;

			if ($this->validate([$step => $restrictions[$step]]))
			{
				$data[$step] = $this->request->getPost($step);

				if ($step !== 'password')
				{
					$data['attempts'] = $values = $model->getLogsInfo($this->request->getIPAddress())['attempts'];
					return view( trim('\Modules\Users\Views\ ') . next($steps), $data);
				}
				else
				{
					// form created
					$hashPass = password_hash($this->request->getPost('password'), PASSWORD_DEFAULT);
					$users    = new User();
					//check for valid user
					$flag = $users->find($this->request->getPost('email'));

					if (isset($flag))
					{
						$flagPass = password_verify($this->request->getPost('password'), $flag['password']);

						if (! $flagPass)
						{
							$data['error'] = 'Compte introuvable. Adresse email ou mot de passe incorrects';
							return view('\Modules\Users\Views\email', $data);
						}
						else
						{
							// ADD MENU, START SESSION
							$userData = [
								'email'     => $flag['email'],
								'password'  => $this->request->getPost('password'),
								'firstname' => $flag['firstname'],
								'lastname'  => $flag['lastname'],
							];
							$session  = session();

							$auth        = new AuthToken();
							$accessToken = bin2hex(random_bytes(16));

							$user = new User();

							$userDb = $user->find($userData['email']);

							$pass = password_verify($userData['password'], $userDb['password']);
							if ($pass && isset($userDb))
							{
								$data = [
									'access_token' => $accessToken,
									'user_id'      => $userDb['user_id'],
								];
							}

							$auth->handleAccessToken($data);
							$sessionData['access_token'] = $auth->findAccessToken($userData)['access_token'];
							$sessionData['ui_color']     = $userDb['ui_color'];
							$session->set($sessionData);

							return redirect()->to('../services');
						}
					}
					else
					{
						$data['error'] = 'Compte introuvable. Adresse email ou mot de passe incorrects';
						return view('\Modules\Users\Views\email', $data);
					}
				}
			}
			else
			{
				$data[$step]        = '';
				$data['validation'] = $this->validator;
				return view( trim('\Modules\Users\Views\ ') . $step, $data);
				/*	if ($this->checkLog())
				{
					$data[$step]        = '';
					$data['validation'] = $this->validator;
					$data['attempts']   = $values = $model->getLogsInfo($this->request->getIPAddress())['attempts'];
					return view('\Modules\Users\Views\login_' . $step, $data);
				}
				else
				{
					$data['time'] = '5 minutes';
					return view('\Modules\Users\Views\login_error', $data);
				}*/
			}
		}
		else
		{
			//non-valid
			$data['email'] = '';
			return view('\Modules\Users\Views\email', $data);
		}
	}

	/**
	 * CheckLog
	 * Processes the number of attempts
	 *
	 * @return null
	 */
	public function checkLog()
	{
		$model  = new Logs();
		$values = $model->getLogsInfo($this->request->getIPAddress());
		$val    = true;
		date_default_timezone_set('Europe/Paris');
		$date2 = new \DateTime();
		if ($values['attempts'] >= 2 && ! empty($values['time']))
		{
			$arr = $values['time'];

			$date     = new \DateTime($arr);
			$interval = $date->diff($date2);
			$offset   = $interval->format('%i');

			if ($offset >= 5)
			{
				$model->removeLogs();
				$data = [];
			}

			$val = false;
		}

		else
		{
			$model->createLog();
			//$this->respondCreated($response);
			$val = true;
		}

		return $val;
	}

	/**
	 * Error
	 * Just the error page
	 *
	 * @return null
	 */
	public function error()
	{
		$data['time'] = '5 minutes';
		return view('\Modules\Users\Views\login_error', $data);
	}

	/**
	 * GenerateErrors
	 *
	 * @param string $validation A string with errors
	 *
	 * @return void
	 */
	public function generateErrors(string $validation)
	{
		$error = new Error();
		echo $error->generateErrors($validation);
	}

	/**
	 * ForgotPassword
	 *
	 * @return null
	 */
	public function forgotPassword()
	{
		helper(['form']);

		if (! $this->request->getPost())
		{
			$data['email'] = '';
			return view('\Modules\Users\Views\Forgot\forgot_password', $data);
		}
		else
		{
			//we submitted a mail from the page
			$restrictions = [
				'email' => [
					'rules'  => 'required|valid_email|is_not_unique[users.email]',
					'errors' => [
						'valid_email'   => 'Adresse e-mail non valide.',
						'required'      => 'Adresse e-mail requise.',
						'is_not_unique' => 'Adresse email introuvable.',
					],
				],
			];

			if ($this->validate(['email' => $restrictions['email']]))
			{
				//search into DB for user
				$users = new User();

				$flag = $users->find($this->request->getPost('email'));

				if (isset($flag))
				{
					$length = 6;
					//generate token
					$token = substr(str_shuffle('0123456789'), 0, $length);

					$to       = $this->request->getPost('email');
					$subject  = 'SISA/ERP - Récupération du mot de passe';
					$message  = 'Vous avez demandé à réinitialiser votre mot de passe, voici votre code :' . PHP_EOL;
					$message .= PHP_EOL . $token . PHP_EOL . PHP_EOL . 'L\'équipe SISA';

					$conf = new Config();
					//	$config = $conf->find('smtp')['value'];
					$email = \Config\Services::email();

					//	Send mail
					$email->setFrom('isaac@sisaweb.com', 'SISA');
					$email->setTo($to);
					$email->setSubject($subject);
					$email->setMessage($message);

					if ($email->send())
					{
						$data['token'] = $token;
						$data['email'] = $to;
						return view('\Modules\Users\Views\Forgot\forgot_verify', $data);
					}
					else
					{
						$data = $email->printDebugger(['headers']);
						print_r($data);
					}
				}
				else
				{
					$data['validation'] = $this->validator;
					$data['email']      = '';
					return view('\Modules\Users\Views\Forgot\forgot_password', $data);
				}
			}
			else
			{
				$data['validation'] = $this->validator;
				$data['email']      = '';
				return view('\Modules\Users\Views\Forgot\forgot_password', $data);
			}
		}
	}

	/**
	 * Verify
	 * Check for verification code
	 *
	 * @return view Verify view
	 */
	public function verify()
	{
		helper(['form']);
		if ($this->request->getPost())
		{
			$token = $this->request->getPost('token');
			$code  = $this->request->getPost('code');

			$restrictions = [
				'code' => [
					'rules'  => 'required|matches[token]',
					'errors' => [
						'matches'  => 'Le code ne correspond pas',
						'required' => 'code requis',
					],
				],

			];
			if (! $this->validate(['code' => $restrictions['code']]))
			{
				//Code does not match token or is empty
				$data['token']      = $token;
				$data['validation'] = $this->validator;
				$data['email']      = $this->request->getPost('email');
				return view('\Modules\Users\Views\Forgot\forgot_verify', $data);
			}
			else
			{
				$data['email'] = $this->request->getPost('email');
				return view('\Modules\Users\Views\Forgot\forgot_reset', $data);
			}
		}
	}

	/**
	 * ResetPassword
	 * Checks password reset
	 *
	 * @return view A view
	 */
	public function resetPassword()
	{
		$model = new User();
		helper(['form']);
		$restrictions = [
			'password'              => [
				'rules'  => 'required|regex_match[/^\S*(?=\S{8})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\W])(?=\S*[\d])\S*$/]',
				'errors' => [
					'regex_match' => 'Le mot de passe doit contenir au moins 1 majuscule, 8 caractères, et un caractère spécial.',
					'required'    => 'Code requis',
				],
			],
			'password_confirmation' => [
				'rules'  => 'required|matches[password]',
				'errors' => [
					'matches'  => 'Les 2 mots de passe ne correspondent pas.',
					'required' => 'Confirmation requise',
				],
			],

		];

		if ($this->request->getPost())
		{
			$mail          = $this->request->getPost('email');
			$password      = $this->request->getPost('password');
			$data['email'] = $mail;
			if (! $this->validate(['password' => $restrictions['password'], 'password_confirmation' => $restrictions['password_confirmation']]))
			{
				$data['validation'] = $this->validator;

				return view('\Modules\Users\Views\Forgot\forgot_reset', $data);
			}
			else
			{
				$password = password_hash($password, PASSWORD_DEFAULT);
				$model->updateByMail($mail, $password);
				return view('\Modules\Users\Views\email');
			}
		}
	}

}
