<?php
/**
 * Api
 *
 * @package   Users\Controllers
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace Modules\Users\Controllers;

use CodeIgniter\RESTful\ResourceController;
use Modules\Users\Models\User;
use \Modules\Users\Models\AuthToken;

/**
 * Api
 *
 * @package   Users\Controllers
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class Api extends ResourceController
{

	/**
	 * ModelName
	 *
	 * @var string
	 */
	protected $modelName = 'Modules\Users\Models\AuthToken';

	/**
	 * Format
	 * Response format
	 *
	 * @var string
	 */
	protected $format = 'json';

	/**
	 * Domain
	 * List of authorized domains
	 *
	 * @var array
	 */
	protected $domain = [
		'erp'   => 'xxxx', //hidden for privacy purposes
		'depot' => 'xxxxx', //hidden for privacy purposes
	];
	 /**
	  * Format
	  *
	  * @var string
	  *
	  * @return reponse All the tokens
	  */
	public function index()
	{
		$app = $this->request->getGet('app');
		if (in_array($app, $this->domain))
		{
			return $this->respond($this->model->findAll());
		}
		else
		{
			return $this->respond('You do not have the authorization to see this', 403);
		}
	}

	/**
	 * Show
	 * Show an element, either token or credentials
	 *
	 * @param mixed $id The info you want to get, use or token
	 *
	 * @return response API GET response
	 */
	public function show(mixed $id = null)
	{
		//$modelName = ($model === 'user' || $model === 'token') ? $model : '';

		$names = [
			'user'  => 'User',
			'token' => 'AuthToken',
		];

		$model = new AuthToken();

		if ($names[$id] === 'User')
		{
			$valid = (null !== $this->request->getGet('access_token')) ? true : false;

			$app = $this->request->getGet('app');
			if ($valid && in_array($app, $this->domain))
			{
				$data['access_token'] = $this->request->getGet('access_token');

				$return = $model->getUser($data);
				if ($return)
				{
					return $this->respond($return);
				}
				else
				{
					return $this->failNotFound('no data found with corresponding token');
				}
			}
			else
			{
				return $this->respond('You do not have the authorization to see this', 403);
			}
		}
		else
		{
			$valid = (null !== $this->request->getGet('email')) && null !== $this->request->getGet('password') ? true : false;
			$app   = $this->request->getGet('app');

			if ($valid && in_array($app, $this->domain))
			{
				$data = [
					'email'    => $this->request->getGet('email'),
					'password' => $this->request->getGet('password'),
				];

				$return = $model->findAccessToken($data);

				if ($return['access_token'])
				{
					return $this->respond($return);
				}
				else
				{
					return $this->failNotFound('no data found with corresponding credentials');
				}
			}
			else
			{
				return $this->respond('You do not have the authorization to see this', 403);
			}
		}
		//echo $modelName;
	}

	/**
	 * Create
	 * Create an access token with existing credentials, or update if token already exists
	 *
	 * @return response HTTP POST response
	 */
	public function create()
	{
		$app = $this->request->getGet('app');
		if (null !== $this->request->getPost('email') && null !== $this->request->getPost('password') && in_array($app, $this->domain))
		{
			$accessToken = bin2hex(random_bytes(16));

			$post['email']    = $this->request->getPost('email');
			$post['password'] = $this->request->getPost('password');

			$user = new User();

			$userDb = $user->find($post['email']);

			$pass = password_verify($post['password'], $userDb['password']);
			if ($pass && isset($userDb))
			{
				$data = [
					'access_token' => $accessToken,
					'user_id'      => $userDb['user_id'],
				];
				return $this->respond($this->model->handleAccessToken($data));
			}
			else
			{
				return $this->respond('Nothing to show', 404);
			}
		}
		else
		{
			throw new \Exception('Missing parameter. Make sure to send email and password', 400);
		}
	}
}
