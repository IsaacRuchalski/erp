<?= $this->extend('../Modules/Templates/portal') ?>
<?= $this->section('head')?>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, Users-scalable=0" name="viewport" />
<meta name="viewport" content="width=device-width" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
   rel="stylesheet">
<link rel="icon" href="path/to/fav.png">
<title>Login</title>
<link href="https://cdn.jsdelivr.net/npm/halfmoon@1.1.1/css/halfmoon-variables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>
<?= $this->section('sidebar')?>
<?= $this->endSection() ?>
<?= $this->section('content') ?>
<div class = "content-wrapper d-flex justify-content-center w-full h-full">
   <div class = "align-self-center w-600 mw-full bg-dark-dm p-20 border rounded">
	  <h2 class="content-title">
		 Vérification
	  </h2>
	  <?= $email ?>
	  <p>Un code vous a été envoyé par mail, veuillez le rentrer ci-dessous : </p>
	  <?php echo form_open('/login/verify', '', ['token' => $token, 'email' => $email]) ?>
	  <div class="form-group">
		 <input type="text" name='code' id="code" class="form-control" placeholder="Code à six chiffres">
	  </div>
	  <input class="btn btn-primary btn-block" type="submit" value="Continuer">
	  </form>
	  <?= $this->include('../Modules/Templates/errorAlert') ?>
   </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('JS')?>
<script src="https://cdn.jsdelivr.net/npm/halfmoon@1.1.1/js/halfmoon.min.js"></script>
<?= $this->endSection() ?>