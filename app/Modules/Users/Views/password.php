<?= $this->extend('../Modules/Templates/portal') ?>
<?= $this->section('head')?>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, Users-scalable=0" name="viewport" />
<meta name="viewport" content="width=device-width" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link rel="icon" href="path/to/fav.png">
<title>Login</title>
<link href="https://cdn.jsdelivr.net/npm/halfmoon@1.1.1/css/halfmoon-variables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>
<?= $this->section('sidebar')?>
<?= $this->endSection() ?>
<?= $this->section('content') ?>
<div class = "content-wrapper d-flex justify-content-center w-full h-full">
	<div class = "align-self-center w-600 mw-full bg-dark-dm p-20 border rounded">
		<?php
			$hidden = [
				'grant_type'    => 'password',
				'client_id'     => 'erp',
				'client_secret' => 'secret_erp',
				'field'         => 'password',
			];

			echo form_open('/login/index', '', $hidden) ?>
		<div class="form-group">
			<label for="email" class="required">Email</label>
			<input type="text" name='email' id="email" class="form-control" placeholder="Email" required="required" value = '<?= $email ?>' readonly="readonly">
		</div>
		<div class="form-group">
			<label for="password" class="required">Mot de passe</label>
			<input type="password" name='password' id="password" class="form-control" placeholder="Mot de passe" required="required">
		</div>
		<input class="btn btn-primary btn-block" type="submit" value="Valider">
		</form>
		<div class = "text-left pt-20">
			<a href = "/login/forgotpassword">Mot de passe oublié</a>
		</div>
		<?= $this->include('../Modules/Templates/errorAlert') ?>
	</div>
</div>
<?= $this->endSection() ?>
<?= $this->section('JS')?>
<script src="https://cdn.jsdelivr.net/npm/halfmoon@1.1.1/js/halfmoon.min.js"></script>
<?= $this->endSection() ?>