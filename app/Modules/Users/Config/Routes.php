<?php
/**
 * Routes
 *
 * @package   Users\Config
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace Modules\Users\Config;

$routes->group(
	'login', ['namespace' => 'Modules\Users\Controllers'], function ($routes) {
		$routes->addRedirect('', 'login/index');
		$routes->match(['get', 'post'], 'forgotpassword', 'Users::forgotPassword');
		$routes->match(['get', 'post'], 'verify', 'Users::verify');
		$routes->match(['get', 'post'], 'reset', 'Users::resetPassword');
		$routes->match(['get', 'post'], 'index', 'Users::index', ['as' => 'User_login']);
		$routes->match(['get', 'post'], 'index', 'Users::index', ['as' => 'User_login']);
		$routes->get('mail', 'Users::index', ['as' => 'Users_mail']);
		$routes->get('password', 'Users::index', ['as' => 'Users_password']);
		$routes->match(['get', 'post'], 'handle', 'Users::handle', ['as' => 'User_handle']);
		$routes->get('mail', 'Users::index');
		$routes->get('error', 'Users::error');
		$routes->get('password', 'Users::login');
	}

);

$routes->resource('api', ['namespace' => 'Modules\Users\Controllers']);
