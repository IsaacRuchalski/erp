<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Meta tags -->

  <?= $this->renderSection('head') ?>
</head>
<body class="with-custom-webkit-scrollbars with-custom-css-scrollbars" data-dm-shortcut-enabled="true" data-sidebar-shortcut-enabled="true" data-set-preferred-mode-onload="true">
  <!-- Modals go here -->
  <!-- Reference: https://www.gethalfmoon.com/docs/modal -->

  <!-- Page wrapper start -->
  <div class="page-wrapper with-navbar with-sidebar">

    <!-- Sticky alerts (toasts), empty container -->
    <!-- Reference: https://www.gethalfmoon.com/docs/sticky-alerts-toasts -->
    <div class="sticky-alerts"></div>

    <!-- Navbar start -->
    <nav class="navbar">
      <?= $this->renderSection('navbar') ?>
    </nav>
    <!-- Navbar end -->

    <!-- Sidebar start -->
    <div class="sidebar">
	<?= $this->renderSection('sidebar') ?>
      <!-- Reference: https://www.gethalfmoon.com/docs/sidebar -->
    </div>
    <!-- Sidebar end -->

    <!-- Content wrapper start -->
    <div class="content-wrapper">

	<?= $this->renderSection('content') ?>

	
    </div>
    <!-- Content wrapper end -->
	
  </div>
  <!-- Page wrapper end -->
  
  <!-- Halfmoon JS -->
  <?= $this->renderSection('JS') ?>
</body>
</html>
<script>

//var inputcolor = document.getElementById("favcolor");
var root = document.documentElement;

root.style.setProperty("--primary-color", '<?= $ui_color ?>');


</script>
<style>



:root{

--primary-color: #33cc00;                                     /* Before: var(--blue-color) */



}

ul li {

list-style : none;

}

ul li p:before {}

.material-icons, .material-icons-outlined {

	line-height: unset;

}

.title:hover , .title{
   font-size: 1.5em;
   text-decoration: none;
   color: var(--primary-color) !important;
   }
</style>
