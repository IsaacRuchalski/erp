<!DOCTYPE html>
<html lang="fr">
<head>

  <?= $this->renderSection('head') ?>

</head>
<body class="with-custom-webkit-scrollbars with-custom-css-scrollbars" data-dm-shortcut-enabled="true" data-set-preferred-mode-onload="true">
  <!-- Modals go here -->
  <!-- Reference: https://www.gethalfmoon.com/docs/modal -->

  <!-- Page wrapper start -->
  <div class="page-wrapper">

    <!-- Sticky alerts (toasts), empty container -->
    <!-- Reference: https://www.gethalfmoon.com/docs/sticky-alerts-toasts -->
    <div class="sticky-alerts"></div>

    <!-- Content wrapper start -->
    <div class="content-wrapper">

	<?= $this->renderSection('content') ?>

    </div>
    <!-- Content wrapper end -->

  </div>
  <!-- Page wrapper end -->

  <!-- Halfmoon JS -->
  <?= $this->renderSection('JS') ?>
</body>
</html>

<style>



:root{

--primary-color: #33cc00;                                     /* Before: var(--blue-color) */



}

ul li {

list-style : none;

}

ul li p:before {}

</style>