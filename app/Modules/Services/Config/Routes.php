<?php
/**
 * Routes
 *
 * @package   Services\Config
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace Modules\Services\Config;

$routes->group(
	'services', ['namespace' => 'Modules\Services\Controllers'], function ($routes) {
		$routes->addRedirect('', 'services/index');
		$routes->match(['get', 'post'], 'index', 'Services::index');
		$routes->match(['get', 'post'], 'logout', 'Services::logout');
		$routes->match(['get', 'post'], 'profile', 'Services::profile');
		$routes->match(['get', 'post'], 'securite', 'Services::profile');
		$routes->match(['get', 'post'], 'settings', 'Services::settings');
		$routes->match(['get', 'post'], '(:segment)', 'Services::select/$1');
	}
);
