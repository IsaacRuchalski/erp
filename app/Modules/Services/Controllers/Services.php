<?php
/**
 * Services
 *
 * @package   Users\Controllers
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace Modules\Services\Controllers;

use Modules\Services\Models\Config;
use Modules\Users\Models\User;
use App\Controllers\BaseController;
use \Modules\Users\Models\AuthToken;
use CodeIgniter\Cookie\Cookie;

const MENU = [

	[
		'label' => 'Services',
		'icon'  => 'developer_board',
		'href'  => 'myservices',
	],
	[
		'label' => 'Placeholder',
		'icon'  => 'space_dashboard',
		'href'  => 'placeholder',
	],

];
/**
 * Services
 *
 * @package   Users\Controllers
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class Services extends BaseController
{
	/**
	 * Index
	 *
	 * @return view index page of services or logout
	 */
	public function index()
	{
		$session = session();

		helper('cookie');

		$cookie = $this->request->getCookie('sisa_token');

		if (null !== $cookie)
		{
			$session->set('access_token', $cookie);
		}
		else
		{
			if ($session->has('access_token'))
			{
				set_cookie('sisa_token', $session->get('access_token'));
			}
			else
			{
				return $this->logout();
			}
		}

		if ($session->has('access_token') && null !== $session->get('access_token'))
		{
			$data         = $this->getSession();
			$data['menu'] = 'menu';
			if (null !== $data)
			{
				return view('Modules\Services\Views\index', $data);
			}
			else
			{
				helper('cookie');

				return $this->logout();
			}
		}
		else
		{
			helper('cookie');

			return $this->logout();
		}
	}

	/**
	 * Logout
	 * Handles Logout
	 *
	 * @return redirect A redirect to login page
	 */
	public function logout()
	{
		$session = session();
		$session->destroy();
		helper('cookie');
		set_cookie('sisa_token', '', time() - 3600);

		return redirect()->to('../login');
	}

	/**
	 * Select
	 * Handle tab selection on sidebar
	 *
	 * @param string $tab The tab name
	 *
	 * @return view the view corresponding to the tab
	 */
	public function select(string $tab)
	{
		$data         = $this->getSession();
		$data['menu'] = 'menu';
		if (null !== $data)
		{
			return view('Modules\Services\Views\services', $data);
		}
		else
		{
			return $this->logout();
		}
	}

	/**
	 * GetSession
	 * Get informations about session
	 *
	 * @return array $data Informations about user from session token
	 */
	public function getSession()
	{
		$session = session();

		$token = $session->get('access_token');

		$user = new AuthToken();

		$arrayToken['access_token'] = $token;

		$data = $user->getUser($arrayToken);

		if (! $data)
		{
			return null;
		}

		helper('cookie');
		set_cookie('sisa_token', $token);
		return $data;
	}

	/**
	 * Profile
	 * Returns profile page
	 *
	 * @return view Link to profile page
	 */
	public function profile()
	{
		$data = $this->getSession();

		$data['menu'] = 'profile';

		return view('Modules\Services\Views\Profile\infos', $data);
	}

	/**
	 * Settings
	 * Returns settings page
	 *
	 * @return view Link to settings page
	 */
	public function settings()
	{
		helper('form');

		$data = $this->getSession();

		$data['menu'] = 'profile';

		if ($this->request->getPost())
		{
			$mail = $this->getSession()['email'];
			$user = new User();

			$color = $this->request->getPost('inputcolor');

			$user->setUserColor($mail, $color);

			return redirect()->to('../services');
		}
		else
		{
			return view('Modules\Services\Views\Profile\settings', $data);
		}
	}
}
