<?php
/**
 * Config
 *
 * @package   Services\Models
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace Modules\Services\Models;

use CodeIgniter\Model;

/**
 * Config
 *
 * @package   Services\Models
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class Config extends Model
{
	protected $table         = 'parameters';
	protected $primaryKey    = 'name';
	protected $allowedFields = [
		'name',
		'value',
	];

}
