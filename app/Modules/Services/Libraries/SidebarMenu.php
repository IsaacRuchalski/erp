<?php
/**
 * Error
 *
 * @package   Users\Libraries
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace Modules\Services\Libraries;

/**
 * Error
 *
 * @package   Users\Libraries
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class SidebarMenu
{

	/**
	 * Menu
	 * Main menu
	 *
	 * @var array
	 */
	public $menu = [
		[
			'label' => 'Services',
			'icon'  => 'developer_board',
			'href'  => 'myservices',
		],
		[
			'label' => 'Placeholder',
			'icon'  => 'space_dashboard',
			'href'  => 'placeholder',
		],
	];

	/**
	 * Profile
	 * Profile menu
	 *
	 * @var array
	 */
	public $profile = [
		[
			'label' => 'Accueil',
			'icon'  => 'home',
			'href'  => '../',
		],
		[
			'label' => 'Informations générales',
			'icon'  => 'info',
			'href'  => 'profile',
		],
		[
			'label' => 'Sécurité',
			'icon'  => 'lock',
			'href'  => 'securite',
		],
	];

	/**
	 * Generate
	 * Generates a menu based on the parameter.
	 *
	 * @param string $menu Menu name
	 *
	 * @return string html A HTML-formatted string containing all  menu items corresponding to the menu string
	 */
	public function generate(string $menu)
	{
		$html = '<div class = "sidebar-menu"><div class = "sidebar-content">';
		foreach ($this->$menu as $tab)
		{
			 $html .= '<a href = "' . $tab['href'] . '" class = "sidebar-link sidebar-link-with-icon">';
			 $html .= '<span class = "material-icons align middle mr-10">' . $tab['icon'] . '</span>';
			 $html .= $tab['label'] . '</a>';
		}

		$html .= '</div></div>';

		return $html;
	}

}
