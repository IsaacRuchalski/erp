<?= $this->extend('../Modules/Templates/template') ?>
<?= $this->section('head')?>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta name="viewport" content="width=device-width" />
<link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
<link rel="icon" href="path/to/fav.png">
<title></title>
<link href="https://cdn.jsdelivr.net/npm/halfmoon@1.1.1/css/halfmoon-variables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>
<?= $this->section('navbar') ?>
<div class = "d-flex justify-content-between w-full  align-items-center">
   <a href = "index" class = "title">
      <div class = "w-auto h-full d-flex justify-content-center align-items-center">
      <!-- <img src =  "<?= base_url(); ?>\assets\logo.svg" class = 'img-fluid w-25'>-->
         <p class = "ml-10">ERP</p>
      </div>
   </a>
   <div class="dropdown">
      <button class="btn btn-primary" data-toggle="dropdown" type="button" id="dropdown-toggle-btn-1" aria-haspopup="true" aria-expanded="false">
      Profil
      </button>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-toggle-btn-1">
         <h4 class="dropdown-header"><?= $firstname . ' ' . $lastname ?></h4>
         <h6 class="dropdown-header"><?= $email ?></h6>
         <a href="profile" class="dropdown-item">  <span class="material-icons align-middle">person</span> Mon profil</a>
         <a href="settings" class="dropdown-item"> <span class="material-icons align-middle">settings</span> Paramètres</a>
         <div class="dropdown-divider"></div>
         <div class="dropdown-content">
            <a href = "logout"><button class="btn btn-square btn-primary" type="button"><span class="material-icons d-inline-flex justify-content-center">logout</span></button></a>
            <button class="btn btn-square btn-primary" type="button" onclick="halfmoon.toggleSidebar()"><span class="material-icons d-inline-flex justify-content-center">menu</span></button>
            <button class="btn btn-square btn-primary" type="button" onclick="halfmoon.toggleDarkMode()"><span class="material-icons-outlined d-inline-flex">dark_mode</span></button>
         </div>
      </div>
   </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('sidebar')?>
<?= $this->include('../Modules/Templates/sidebarMenu') ?>
<?= $this->endSection() ?>
<?= $this->section('content') ?>
<nav aria-label="Breadcrumb">
   <ul class="breadcrumb m-20">
      <li class="breadcrumb-item"><a href="index">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page"><a href="#">Services</a></li>
   </ul>
</nav>
<div class="container-fluid">
   <div class="row h-200">
      <div class="col-md d-flex flex-column justify-content-center m-10 border rounded align-items-center">
         <a href = "https://depot.sisa.ovh/login/index" class = "w-full h-full d-flex text-center align-items-center justify-content-center">
            <svg class = 'img-fluid rounded w-150' viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
               <g id="Data_sharing_global">
                  <path d="M428.1561,365.8882H297.9667a9.4783,9.4783,0,0,0-9.4678,9.4678v35.205a9.4783,9.4783,0,0,0,9.4678,9.4678H428.1561a9.4783,9.4783,0,0,0,9.4678-9.4678V375.356A9.4783,9.4783,0,0,0,428.1561,365.8882Zm-98.3115,32.57H317.6464a5.5,5.5,0,0,1,0-11h12.1982a5.5,5.5,0,0,1,0,11Z"/>
                  <path d="M428.1561,437.8657H297.9667a9.4783,9.4783,0,0,0-9.4678,9.4678v35.2051a9.4783,9.4783,0,0,0,9.4678,9.4677H428.1561a9.4783,9.4783,0,0,0,9.4678-9.4677V447.3335A9.4783,9.4783,0,0,0,428.1561,437.8657Zm-98.3115,32.57H317.6464a5.5,5.5,0,0,1,0-11h12.1982a5.5,5.5,0,0,1,0,11Z"/>
                  <path d="M428.1561,293.91H297.9667a9.4783,9.4783,0,0,0-9.4678,9.4677v35.2051a9.4783,9.4783,0,0,0,9.4678,9.4678H428.1561a9.4783,9.4783,0,0,0,9.4678-9.4678V303.3774A9.4783,9.4783,0,0,0,428.1561,293.91Zm-98.3115,32.57H317.6464a5.5,5.5,0,0,1,0-11h12.1982a5.5,5.5,0,0,1,0,11Z"/>
                  <path d="M375.6112,134.7729A119.6045,119.6045,0,1,0,256.0067,254.3774,119.74,119.74,0,0,0,375.6112,134.7729ZM214.2343,36.6987a99.3844,99.3844,0,0,0-10.4737,14.8242A132.0834,132.0834,0,0,0,193.7133,73.314H168.95A107.2445,107.2445,0,0,1,214.2343,36.6987ZM161.0668,86.314h28.6074a187.6311,187.6311,0,0,0-6.0015,41.9589H149.6063A105.8291,105.8291,0,0,1,161.0668,86.314Zm-11.46,54.9589h34.0664a187.6137,187.6137,0,0,0,6.002,41.96H161.0668A105.8329,105.8329,0,0,1,149.6063,141.2729Zm19.3438,54.96h24.7632a132.0785,132.0785,0,0,0,10.0473,21.791,99.3376,99.3376,0,0,0,10.4737,14.8242A107.2429,107.2429,0,0,1,168.95,196.2329ZM249.5067,240.72c-12.8193-2.5764-24.8862-12.7288-34.5156-29.2434a116.0075,116.0075,0,0,1-7.38-15.2432h41.8955Zm0-57.4866H203.1522a172.3653,172.3653,0,0,1-6.4692-41.96h52.8237Zm0-54.96H196.6835a172.36,172.36,0,0,1,6.4687-41.9589h46.3545Zm0-54.9589H207.6112a116.0075,116.0075,0,0,1,7.38-15.2432c9.6294-16.5147,21.6963-26.667,34.5156-29.2434Zm101.44,109.9189H322.3388a187.6137,187.6137,0,0,0,6.0019-41.96h34.0664A105.8344,105.8344,0,0,1,350.9467,183.2329Zm11.46-54.96H328.3407a187.6022,187.6022,0,0,0-6.0019-41.9589h28.6079A105.8306,105.8306,0,0,1,362.4071,128.2729ZM343.0639,73.314H318.3a132.0439,132.0439,0,0,0-10.0479-21.7911,99.3009,99.3009,0,0,0-10.4736-14.8244A107.2448,107.2448,0,0,1,343.0639,73.314ZM262.5067,28.8274c12.8189,2.5764,24.8863,12.729,34.5147,29.2434a115.9979,115.9979,0,0,1,7.3808,15.2432H262.5067Zm0,57.4866h46.354a172.311,172.311,0,0,1,6.4693,41.9589H262.5067Zm0,54.9589H315.33a172.3434,172.3434,0,0,1-6.4693,41.96h-46.354Zm0,99.4466V196.2329h41.895a116.0593,116.0593,0,0,1-7.38,15.2432C287.393,227.9905,275.3256,238.1431,262.5067,240.72Zm35.2715-7.8711a99.2864,99.2864,0,0,0,10.4736-14.8245A132.0876,132.0876,0,0,0,318.3,196.2329h24.7637A107.2381,107.2381,0,0,1,297.7782,232.8484Z"/>
                  <path d="M102.06,247.8774a24.6434,24.6434,0,0,0-18.1025-23.7246V161.2847A20.0348,20.0348,0,0,1,103.97,141.2729h11.4346a6.5,6.5,0,0,0,0-13H103.97a33.0486,33.0486,0,0,0-33.0117,33.0118v62.8681A24.6005,24.6005,0,1,0,102.06,247.8774Zm-36.2041,0A11.6021,11.6021,0,1,1,77.4579,259.479,11.6151,11.6151,0,0,1,65.8563,247.8774Z"/>
                  <path d="M440.6517,224.1609l.3091-62.8762a33.0486,33.0486,0,0,0-33.0117-33.0118H396.5145a6.5,6.5,0,0,0,0,13h11.4346a20.028,20.028,0,0,1,20.0117,19.98l-.31,62.8926a24.604,24.604,0,1,0,13,.0159Zm-6.5278,35.3181a11.6021,11.6021,0,1,1,11.6026-11.6016A11.6143,11.6143,0,0,1,434.1239,259.479Z"/>
                  <path d="M103.7821,381.4468h41.833a6.5,6.5,0,1,0,0-13h-41.833a6.5,6.5,0,0,0,0,13Z"/>
                  <path d="M103.7821,405.88h41.833a6.5,6.5,0,0,0,0-13h-41.833a6.5,6.5,0,0,0,0,13Z"/>
                  <path d="M97.2821,423.813a6.5,6.5,0,0,0,6.5,6.5h67.2754a6.5,6.5,0,0,0,0-13H103.7821A6.5,6.5,0,0,0,97.2821,423.813Z"/>
                  <path d="M224.7733,317.7485H186.9706l-24.5918-24.5918a6.5037,6.5037,0,0,0-4.5967-1.9033H85.7138a14.772,14.772,0,0,0-14.7559,14.7549v144.708a14.772,14.772,0,0,0,14.7559,14.7549h16.3291v14.8281a14.7237,14.7237,0,0,0,14.707,14.707H224.7733a14.724,14.724,0,0,0,14.708-14.707V332.4565A14.7242,14.7242,0,0,0,224.7733,317.7485Zm-60.4912-4.3027,21.9219,21.9219H164.2821Zm31.1143,137.27a1.7567,1.7567,0,0,1-1.7549,1.7549H85.7138a1.7576,1.7576,0,0,1-1.7559-1.7549V306.0083a1.7576,1.7576,0,0,1,1.7559-1.7549h65.5683v37.6143a6.5,6.5,0,0,0,6.5,6.5h37.6143Z"/>
               </g>
            </svg>
            <p class = 'item text-white'>DEPOT</p>
         </a>
      </div>
      <div class="col-md d-flex flex-column justify-content-center m-10 border rounded align-items-center">
         <a href = "https://depot.sisa.ovh/login/index" class = "w-full h-full d-flex text-center align-items-center justify-content-center">
            <svg class = 'img-fluid rounded w-150' viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
               <g>
                  <g>
                     <path d="M243.225,333.382c-13.6,0-25,11.4-25,25s11.4,25,25,25c13.1,0,25-11.4,24.4-24.4
                        C268.225,344.682,256.925,333.382,243.225,333.382z"/>
                     <path d="M474.625,421.982c15.7-27.1,15.8-59.4,0.2-86.4l-156.6-271.2c-15.5-27.3-43.5-43.5-74.9-43.5s-59.4,16.3-74.9,43.4
                        l-156.8,271.5c-15.6,27.3-15.5,59.8,0.3,86.9c15.6,26.8,43.5,42.9,74.7,42.9h312.8
                        C430.725,465.582,458.825,449.282,474.625,421.982z M440.625,402.382c-8.7,15-24.1,23.9-41.3,23.9h-312.8
                        c-17,0-32.3-8.7-40.8-23.4c-8.6-14.9-8.7-32.7-0.1-47.7l156.8-271.4c8.5-14.9,23.7-23.7,40.9-23.7c17.1,0,32.4,8.9,40.9,23.8
                        l156.7,271.4C449.325,369.882,449.225,387.482,440.625,402.382z"/>
                     <path d="M237.025,157.882c-11.9,3.4-19.3,14.2-19.3,27.3c0.6,7.9,1.1,15.9,1.7,23.8c1.7,30.1,3.4,59.6,5.1,89.7
                        c0.6,10.2,8.5,17.6,18.7,17.6c10.2,0,18.2-7.9,18.7-18.2c0-6.2,0-11.9,0.6-18.2c1.1-19.3,2.3-38.6,3.4-57.9
                        c0.6-12.5,1.7-25,2.3-37.5c0-4.5-0.6-8.5-2.3-12.5C260.825,160.782,248.925,155.082,237.025,157.882z"/>
                  </g>
               </g>
            </svg>
            <p class = 'item text-white'>EN COURS</p>
         </a>
      </div>
      <div class="col-md d-flex flex-column justify-content-center m-10 border rounded align-items-center">
         <a href = "https://depot.sisa.ovh/login/index" class = "w-full h-full d-flex text-center align-items-center justify-content-center">
            <svg class = 'img-fluid rounded w-150' viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
               <g>
                  <g>
                     <path d="M243.225,333.382c-13.6,0-25,11.4-25,25s11.4,25,25,25c13.1,0,25-11.4,24.4-24.4
                        C268.225,344.682,256.925,333.382,243.225,333.382z"/>
                     <path d="M474.625,421.982c15.7-27.1,15.8-59.4,0.2-86.4l-156.6-271.2c-15.5-27.3-43.5-43.5-74.9-43.5s-59.4,16.3-74.9,43.4
                        l-156.8,271.5c-15.6,27.3-15.5,59.8,0.3,86.9c15.6,26.8,43.5,42.9,74.7,42.9h312.8
                        C430.725,465.582,458.825,449.282,474.625,421.982z M440.625,402.382c-8.7,15-24.1,23.9-41.3,23.9h-312.8
                        c-17,0-32.3-8.7-40.8-23.4c-8.6-14.9-8.7-32.7-0.1-47.7l156.8-271.4c8.5-14.9,23.7-23.7,40.9-23.7c17.1,0,32.4,8.9,40.9,23.8
                        l156.7,271.4C449.325,369.882,449.225,387.482,440.625,402.382z"/>
                     <path d="M237.025,157.882c-11.9,3.4-19.3,14.2-19.3,27.3c0.6,7.9,1.1,15.9,1.7,23.8c1.7,30.1,3.4,59.6,5.1,89.7
                        c0.6,10.2,8.5,17.6,18.7,17.6c10.2,0,18.2-7.9,18.7-18.2c0-6.2,0-11.9,0.6-18.2c1.1-19.3,2.3-38.6,3.4-57.9
                        c0.6-12.5,1.7-25,2.3-37.5c0-4.5-0.6-8.5-2.3-12.5C260.825,160.782,248.925,155.082,237.025,157.882z"/>
                  </g>
               </g>
            </svg>
            <p class = 'item text-white'>EN COURS</p>
         </a>
      </div>
      <div class="col-md d-flex flex-column justify-content-center m-10 border rounded align-items-center">
         <a href = "https://depot.sisa.ovh/login/index" class = "w-full h-full d-flex text-center align-items-center justify-content-center">
            <svg class = 'img-fluid rounded w-150' viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
               <g>
                  <g>
                     <path d="M243.225,333.382c-13.6,0-25,11.4-25,25s11.4,25,25,25c13.1,0,25-11.4,24.4-24.4
                        C268.225,344.682,256.925,333.382,243.225,333.382z"/>
                     <path d="M474.625,421.982c15.7-27.1,15.8-59.4,0.2-86.4l-156.6-271.2c-15.5-27.3-43.5-43.5-74.9-43.5s-59.4,16.3-74.9,43.4
                        l-156.8,271.5c-15.6,27.3-15.5,59.8,0.3,86.9c15.6,26.8,43.5,42.9,74.7,42.9h312.8
                        C430.725,465.582,458.825,449.282,474.625,421.982z M440.625,402.382c-8.7,15-24.1,23.9-41.3,23.9h-312.8
                        c-17,0-32.3-8.7-40.8-23.4c-8.6-14.9-8.7-32.7-0.1-47.7l156.8-271.4c8.5-14.9,23.7-23.7,40.9-23.7c17.1,0,32.4,8.9,40.9,23.8
                        l156.7,271.4C449.325,369.882,449.225,387.482,440.625,402.382z"/>
                     <path d="M237.025,157.882c-11.9,3.4-19.3,14.2-19.3,27.3c0.6,7.9,1.1,15.9,1.7,23.8c1.7,30.1,3.4,59.6,5.1,89.7
                        c0.6,10.2,8.5,17.6,18.7,17.6c10.2,0,18.2-7.9,18.7-18.2c0-6.2,0-11.9,0.6-18.2c1.1-19.3,2.3-38.6,3.4-57.9
                        c0.6-12.5,1.7-25,2.3-37.5c0-4.5-0.6-8.5-2.3-12.5C260.825,160.782,248.925,155.082,237.025,157.882z"/>
                  </g>
               </g>
            </svg>
            <p class = 'item text-white'>EN COURS</p>
         </a>
      </div>
   </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('JS') ?>
<script src="https://cdn.jsdelivr.net/npm/halfmoon@1.1.1/js/halfmoon.min.js"></script>

<style>
   svg,path {
   fill: var(--primary-color);
   }
   .item {
   display: none;
   }
   .col-md a{
   text-decoration: none;
   color: unset;
   }
   .col-md{
   transition: 200ms;
   }
   .col-md:hover svg {
   filter: blur(2px);
   }
   .col-md:hover a {
   color: unset;
   }
   .col-md:hover {
   background-color: #22222244;
   -webkit-box-shadow: 0px 0px 78px 16px rgba(31, 31, 31, 0.7) inset;
   -moz-shadow: 0px 0px 78px 16px rgba(31, 31, 31, 0.7) inset;
   box-shadow: 0px 0px 78px 16px rgba(31, 31, 31, 0.7) inset;
   }
   .col-md:hover .item{
   display: block;
   font-size: 24px;
   position: absolute;
   }
   .item:hover {
   text-decoration: underline;
   }
   .title:hover , .title{
   font-size: 1.5em;
   text-decoration: none;
   color: var(--primary-color) !important;
   }
</style>
<?= $this->endSection() ?>