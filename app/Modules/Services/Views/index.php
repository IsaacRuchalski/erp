<?= $this->extend('../Modules/Templates/template') ?>
<?= $this->section('head')?>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta name="viewport" content="width=device-width" />
<link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
<link rel="icon" href="path/to/fav.png">
<title></title>
<link href="https://cdn.jsdelivr.net/npm/halfmoon@1.1.1/css/halfmoon-variables.min.css" rel="stylesheet" />
<?= $this->endSection() ?>
<?= $this->section('navbar') ?>
<div class = "d-flex justify-content-between w-full  align-items-center">
   <a href = "index" class = "title">
      <div class = "w-auto h-full d-flex justify-content-center align-items-center">
        <!-- <img src =  "<?= base_url(); ?>\assets\logo.svg" class = 'img-fluid w-25'>-->
         <p class = "ml-10">ERP</p>
      </div>
   </a>
   <div class="dropdown">
      <button class="btn btn-primary" data-toggle="dropdown" type="button" id="dropdown-toggle-btn-1" aria-haspopup="true" aria-expanded="false">
      Profil
      </button>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-toggle-btn-1">
         <h4 class="dropdown-header"><?= $firstname . ' ' . $lastname ?></h4>
         <h6 class="dropdown-header"><?= $email ?></h6>
         <a href="profile" class="dropdown-item">  <span class="material-icons align-middle">person</span> Mon profil</a>
         <a href="settings" class="dropdown-item"> <span class="material-icons align-middle">settings</span> Paramètres</a>
         <div class="dropdown-divider"></div>
         <div class="dropdown-content">
            <a href = "logout"><button class="btn btn-square btn-primary" type="button"><span class="material-icons d-inline-flex justify-content-center">logout</span></button></a>
            <button class="btn btn-square btn-primary" type="button" onclick="halfmoon.toggleSidebar()"><span class="material-icons d-inline-flex justify-content-center">menu</span></button>
            <button class="btn btn-square btn-primary" type="button" onclick="halfmoon.toggleDarkMode()"><span class="material-icons-outlined d-inline-flex">dark_mode</span></button>
         </div>
      </div>
   </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('sidebar')?>

<?= $this->include('../Modules/Templates/sidebarMenu') ?>
<?= $this->endSection() ?>
<?= $this->section('content') ?>


<?= $this->endSection() ?>
<?= $this->section('JS') ?>
<script src="https://cdn.jsdelivr.net/npm/halfmoon@1.1.1/js/halfmoon.min.js"></script>

<?= $this->endSection() ?>